from pyramid.scaffolds.template import Template # API

class RestProjectTemplate(PyramidTemplate):
    _template_dir = 'scaffolds'
    summary = 'Pyramid rest project'

